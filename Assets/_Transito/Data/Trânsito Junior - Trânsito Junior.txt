	Pergunta	Resposta errada 1	Resposta errada 2	Resposta errada 3	Resposta certa
1	Qual é o nome do local onde caminhamos e que fica nas  laterais da rua?	Faixa de segurança	Ciclovia	Ponte	Calçada
2	Qual objeto tem luzes com três cores diferentes? 	Placas de sinalização	Uniforme	Farol do veículo	Semáforo
3	Qual a cor do semáforo que significa Pare?	Amarelo	Verde	Azul	Vermelha
4	Qual a cor do semáforo que significa Atenção?	Vermelho	Verde	Branco	Amarelo
5	Qual a cor do semáforo que significa Siga?	Amarelo	Vermelho	Azul	Verde
6	Em qual banco do carro as crianças devem ser transportadas?	Banco da frente	Banco do motorista	Porta malas	No Banco de trás
7	Qual é o item de segurança que todos os ocupantes do carro devem utilizar?	GPS	Capacetes	Extintor	Cinto de Segurança
8	Para atravessar a rua devemos utilizar sempre a:	Opção mais fácil	Via rápida	Faixa de limite	Faixa de pedestres
9	É certo ou errado atravessar a rua correndo?	Certo caso tenha pressa	Correto sempre	Certo em alguns casos	Errado pois devemos atravessar com cuidado e atenção
10	Antes de atravessar a rua devemos olhar para:	O carro mais próximo	As pessoas ao seu redor	Apenas para frente	Todos os lados
11	É certo ou errado colocar os braços para fora quando se está em um veículo?	Correto pois é divertido	É certo pois estando dentro do veículo estamos seguros	Certo sempre	Errado pois podemos ocasionar / sofrer acidentes
12	Ao andar de bicicleta devemos usar sempre os:	Equipamentos de filmagem	Fones de ouvido	Óculos de sol	Equipamentos de segurança.
13	É certo ou errado passar o cinto de segurança por baixo do braço?	Certo, desde que você esteja de cinto	Correto pois assim não machuca seu braço.	Errado, o cinto deve estar na altura do seu pescoço	Errado, o cinto deve estar ajustado de acordo com sua altura.
14	Quando um objeto cai quando você está atravessando a rua, você deve parar para juntar?	Sim	Sim pois não devemos deixar nada no chão	Sim, pois o objeto é seu	Não pois podemos ocasionar / sofrer acidentes
15	É certo ou errado falar ao celular enquanto dirige?	Certo, desde que seja rápido	Correto, se estiver no viva vóz	Não, devemos responder pelo Wattsapp	Errado, pois a atenção do motorista deve estar somente na direção.
16	O adulto deve segurar a criança por qual parte do corpo ao atravessar a rua? 	Pelo pescoço	Pela mão	Pelo ombro	pelo pulso.
17	É certo ou errado ficar em pé entre os bancos do carro?	Certo pois é divertido	Certo	Errado, podemos ficar em cima dos bancos	Errado, devemos estar sentados e usando o cinto de segurança
18	Se a rua não tiver faixa ou semáforo, como devemos atravessar a rua?	Em diagonal	Em zigue e zague	Correndo	Em linha reta
19	É certo ou errado transportar pessoas no porta-malas?	Certo, caso não haja bancos vagos	Certo, se o porta malas ficar aberto	Errado, somente animais	Errado
20	Se um brinquedo cair na rua, devemos pedir ajuda para quem?	Para o amiguinho (a)	Para o prefeito	Não pedir ajuda para ninguém	para um adulto.
21	Em que lugar as crianças pequenas, até 4 anos, devem ser transportadas?	No porta malas	No banco da frente	No banco traseiro 	Na cadeirinha.
22	Os agentes de trânsito são autoridades e devem ser: 	Vaiados	Desreispeitados	Xingados	Respeitados
23	É certo ou errado brincar no meio fio? 	Certo pois é divertido	Somente de equilíbrio	Certo mais com cuidado	Errado
24	Quando não há calçada, onde devemos andar?	No meio da rua	Em qualquer lugar	Equilibrando-se no meio fio	Nas laterais da rua
25	Ao entrar no ônibus, é certo ou errado parar nos degraus?	Certo	Certo se o ônibus estiver lotado	Errado, podemos sentar nos degraus	Errado
26	É certo ou errado falar com o motorista do ônibus enquanto ele está dirigindo?	Certo se ele for seu amigo	Somente para pedir informações	Somente por telefone	Errado
27	Ao subir ou descer do ônibus devemos esperar que ele:	Abra a porta em movimento	Buzine	Mova-se devagar	Pare completamente
28	É certo ou errado atravessar na frente do ônibus?	Sim, posso atravessar em qualquer lado.	Errado, deve-se atravessar atrás do veículo.	Sim, é livre atravessar	Errado, deve-se esperar o ônibus sair do local e atravessar na faixa.
29	Qual é o nome do veículo onde cabem aproximadamente 29 pessoas sentadas?	Ambulância	Taxi	Carro	ônibus
30	Qual é a cor da faixa da van que leva as crianças para as escolas?	Azul	Verde	Vermelha	Amarelo
31	Os pneus da bicicleta sempre devem estar:	Lisos	Vazios	Limpos	cheios
32	Os veículos e pedestres podem andar pela cidade em paz e sem causar	Trânsito	Filas	Congestionamentos	Acidentes
33	As ruas são de todos, ou seja, elas são:	Privadas	Exclusivas	Liberadas	Públicas
34	Para não haver confusão no trânsito, todos devemos respeitar:	Conselhos 	Os mais velhos	Os amigos	regras ou leis de trânsito
35	As cores do semáforo são iguais em todos os países. Certo ou errado?	Errado cada pais adota as suas cores	Errado	Certo mas podem ser diferentes	Certo
36	A poluição gerada por um ônibus equivale a quantos carros aproximadamente?	Aproximadamente 10 carros	Aproximadamente 50 carros	Aproximadamente 1 carro 	Aproximadamente 3 carros.
37	Quem são mais rápidos, os veículos ou as pessoas?	Só veículos novos	Só veículos grandes	As pessoas	Os veículos
38	Veículos e pessoas devem andar separados. Qual é o lugar de cada um?	Veiculo na calçada e pessoas na estrada	Ambos na estrada	Ambos na calçada	veículos na estrada e pessoas na calçada.
39	Pessoas a pé devem andar na:	Rua	Estrada	Ciclovia	Calçada
40	Pessoas com carrinhos de bebê ou cadeira de rodas devem  andar na:	Rua	Estrada	Ciclovia	Calçada
41	Qual é o único momento que devemos descer da calçada?	Para ajuntar objetos na rua	Para pararmos um veículo	Para correr entre os carros	para atravessar a rua
42	Os veículos não podem ficar parados sobre a:	Estradas	Pontes	Avenidas	faixa de pedestres
43	Quando há um semáforo para pedestres no cruzamento, devemos esperar qual luz acender para atravessar?	Vermelha	Amarela	Branco	Verde
44	Só podemos atravessar a rua quando os veículos estiverem totalmente:	Em movimento	Desligados	Limpos	Parados
45	Em qual lugar da rua os carros diminuem a velocidade,  facilitando assim a travessia?	No meio	Nos cantos	No início	no início e no fim
46	O pedestre possui duas regras principais. Uma é ver e a outra é:	Correr	Sorrir	Comprimentar	Ser visto
47	É melhor atravessar a rua em lugares abertos ou saindo de trás de árvores, lixeiras ou carros estacionados?	Saindo de trás de árvores	Saindo de trás de carros	Sempre correndo	em lugares abertos
48	Ao atravessar a rua, é necessário que você seja visto por quem?	Pelos outros pedestres	Pelo amigo	Pelas câmeras de segurança	Pelos motoristas
49	As ruas são excelentes locais para brincadeiras. Certo ou errado?	Certo	Certo, pois posso correr	Errado, melhor entre os carros 	Errado
50	Os acidentes acontecem por falta de:	Velocidade	Freio nos carros	Poderes especiais para voar	Atenção
51	É certo ou errado correr atrás da bola quando esta foi para o meio da rua?	Certo pois a bola é sua	Correto se você correr bem rápido	Certo	Errado
52	O que significa a cor vermelha? 	Siga	Atenção	Dance	Pare 
53	O que significa a cor amarela?	Pare	Siga	Corra	Atenção
54	O que significa a cor verde?	Pare	Atenção	Preserve a natureza	Siga
55	Andando de bicicleta você continua sendo um pedestre?	Sim	Sim, um pedestre condutor	Não, um motorista	Não, um ciclista
56	Quem anda mais rápido, o pedestre ou o ciclista?	Pedestre	Os dois andam na mesma velocidade	Os dois tem a mesma velocidade	Ciclista
57	Na rua, a bicicleta é um brinquedo ou um veículo:	Brinquedo	Brinquedo, e tem preferência	Só bicicletas menores são brinquedos	Veículo  
58	Onde o ciclista deve andar?	No meio da rua	Onde ele quiser	Na calçada	no lado direito da rua, próximo ao meio fio.
59	Qual item de segurança é obrigatório o uso pelo ciclista?	Luvas	Roupas especiais	Óculos de sol	o capacete
60	Capacete, cotoveleiras, joelheiras ou luvas são equipamentos de:	Passeio	Moda	Beleza	segurança ou proteção
61	É certo ou errado fazer manobras radicais na frente dos carros?	Certo	Certo e divertido	Errado, manobras devem ser feitas no trânsito	errado
62	O ciclista também deve respeitar o semáforo e a faixa de:	Apoio	Avisos	Velocidade	Pedestre
63	Além do capacete, diga mais  três itens importantes para segurança dos ciclistas:	Fones de ouvido, óculos de sol e meias	Cirene, óculos de sol e rádio portátil	Celular, Fones de ouvido e capa	farol, refletor e campainha.
64	Quem dirige o carro é o:	Pedestre	Ciclista	Motoqueiro	Motorista
65	Para dirigir é necessário ter qual documento?	Carteira de Identidade	Documento com foto	Certidão de nascimento	Carteira Nacional de Habilitação.
66	Qual é a idade mínima para poder tirar a Carteira Nacional de Habilitação?	16 anos	20 anos	15 anos	18 anos
67	Para tirar a Carteira Nacional de Habilitação o candidato deve conhecer bem as: 	Ruas da cidade	Placas de sinalização	Cores do semáforo	leis ou regras de trânsito.
68	É certo ou errado jogar lixo pela janela do carro?	Certo	Certo, caso não tenha lixeiras perto	Errado. Pare o carro e jogue no chão	Errado
69	O que devemos ter dentro do carro para guardar papéis de balas, latas ou embalagens vazias?	Uma caixa de papelão	Nada, jogue pela janela	Não é necessário pois você pode jogar na rua	um lixeirinho ou  saco plástico..
70	Quem fica sentado quieto no seu banco é considerado um bom ou ruim passageiro?	Passageiro ruim	Um pedestre	Um passageiro chato	bom passageiro.
71	Gritos ou movimentos bruscos podem assustar o:	Cachorro	Os pedestres nas ruas	Os ciclistas	o motorista
72	Choros e manhas podem assustar o	Amigo	A mamãe	Os ciclistas	o motorista
73	Se você ficar em pé, em cima do banco, poderá se machucar seriamente no caso de um acidente. A afirmação está certa  ou errada?	Errado  	Errada pois dentro do carro você esta seguro	Incorreta	Certa
74	Quem dirige pode ficar prestando atenção em você?	Sim, pode	Pode prestar atenção em qualquer coisa	Não, pode apenas ficar no celular	não, deve prestar atenção nos carros e pedestres.
75	É certo ou errado ficar com os braços ou cabeça para fora da janela do carro?	Certo	Certo e divertido	Errado, somente a cabeça pode ficar para fora da janela	errado
76	O que acontece com quem não estiver usando o cinto de segurança durante um acidente? 	Nada	Sentir um pequeno desconforto	Não perceber o acidente	pode ser lançado para fora do carro.
77	Somente o motorista precisa usar o cinto de segurança no veículo. A afirmação está certa ou errada?	Correta	Esta correta pois ele é o mais importante	Certa	errada. Todos devem usar o cinto de segurança.
78	Somente os ocupantes dos bancos da frente precisam usar o cinto de segurança no  veículo. A afirmação está certa ou errada?	Certa, pois eles estão na frente	Correto	Sim, pois na frente é mais perigoso	errada. Todos devem usar o cinto de segurança.
79	Como saber se eu já tenho tamanho para usar o cinto de segurança de três pontas?	Se eu conseguir engatar.	Se ele estiver bem apertado.	Se ele parrar no meu pescoço. 	Se ele passar no meio do meu peito.
80	Se o cinto passar pela altura da garganta ou cabeça, qual acessório devo usar para que o cinto fique na altura correta??	Capacete	Almofada	Travesseiro	assento de elevação.
81	Os bancos da frente foram feitos para os adultos. A afirmação está certa ou errada?	Errado	Foram feitos para qualquer pessoa sentar	Para adultos e animais de estimação	certa
82	A partir de que idade as crianças podem sentar no banco da frente? 	A partir dos 7 anos	A partir dos 6 anos	A partir dos 13 anos	A partir de 10 anos.
83	Qual a parte mais protegida do carro?	A parte da frente	Todas são iguais	As laterais	A parte de trás
84	Como se chamam as ruas onde os veículos somente transitam em um sentido?	Ruas de duplo sentido	Ruas vai e vem	Ruas só vai	Ruas de mão única.
85	Qual é o nome do item que indica as regras a serem seguidas nas ruas?	Outdoors	Faixas	Cartilhas	Placas
86	Que cor são as placas que indicam regras obrigatórias?	Verdes	Amarelas e pretas	Brancas com listras	Brancas com bordas vermelhas.
87	Como chamam as placas que tem borda e são cortadas por um traço vermelho?	Placas de siga	Placas de permitido	Placas de atenção	Placas de proibição
88	Em frente a que tipo de  estabelecimento é proibido buzinar?	Em frente a bares	Em frente a faixa de pedestres	Em qualquer lugar	Em frente a hospitais.
89	O que indicam as placas amarelas?	Área florestal	Localização de postos de combustível	Trânsito lento	Avisos aos motoristas.
90	Com que idade é possível ser caroneiro em uma motocicleta?	5 anos	15 anos	10 anos	7 anos
91	A faixa de pedestres é pintada na rua de que cor? 	Azul	Qualquer cor	Preta	Branca    
92	Como são chamadas as placas azuis e verdes que tem o objetivo de informar?	Placas multicoloridas	Placas Brasileiras	Placas de Vida	Placas auxiliares.
93	Qual a função do agente de trânsito?	Parar o trânsito	Apitar	Cuidar das estradas	Orientar a todos as regras de trânsito.
94	O trânsito causa dois tipos de poluição. Quais são?	Poluição das águas	Não causa nenhuma poluição	Poluição sonora	Poluição atmosférica e poluição sonora.
95	O que os motoristas devem fazer com os seus carros para que eles sempre estejam em dia para serem usados com segurança?	Lavar toda a semana	Calibrar os pneus	Não precisa fazer nada além de andar	manutenção  preventiva.
96	Como é o nome do conjunto de leis e regras que todos devem respeitar no trânsito?	Regras de Direção	Conjunto de regras a se fazer	Lista de deveres de trânsito	Código de Trânsito Brasileiro.
97	Os motores dos carros devem  estar sempre regulados para evitar a: 	Descarga elétrica	Alta velocidade	Falha de direção	Poluição  
98	Quem deve sempre acompanhar uma criança ao sair de casa?	Seu animal de estimação	Outra criança	Ninguem, ela pode sair sozinha sempre	Um adulto
99	Para que serve a sinalização de trânsito?	Para iluminar as ruas	Para os pedestres não serem atropelados	Para prender infratores	Para orientar e proteger.
100	Qual é o lado correto para desembarcar de um veículo que faz o transporte escolar?	Qualquer lado	Não existe lado correto	Ambos os lados	No lado da calçada.
101	Algumas pessoas tem preferência dentro dos ônibus e ao embarcar ou desembarcar. Quem são?	Não existe preferências	Somente idosos	Crianças	 idosos, gestantes, crianças de colo e portadores de deficiência. 
102	Quem está autorizado a dirigir um veículo?	Qualquer pessoa que saiba dirigir	Somente adultos	Todas as pessoas	quem possuir a Carteira Nacional de Habilitação.
103	É permitido dirigir após ter consumido bebida alcoólica?	Sim, se for pouca bebida	Sim, se ao motorista beber água antes	É permitido	Não, não é permitido
104	Porque os motoristas recebem uma multa?	Para receber o endereço da delegacia	Para levar um castigo	Para ter do que se lembrar	Porque não cumpriram uma ou mais regras de trânsito.
105	Qual é o objetivo da multa?	Identificar o motorista	Dar um castigo	Prender o carro	Evitar que o mesmo erro seja feito novamente.
106	Como os animais devem ser transportados dentro dos veículos? 	Soltos	Nunca podem ser transportados	Com o cinto de segurança normal	Em caixas especiais.
107	Os animais podem viajar ser transportados no colo, nos pés ou soltos dentro do carro?	Sim	Podem viajar no colo com o cinto de segurança	Não, somente no banco presos pela coleira	Não. Somente em caixas específicas.
108	Diga dois cuidados que devemos ter para passear com animais na rua.	Dar banho e comida antes	Pentear e perfumar	Avisa-lo sobre os cuidados ao atravessar as ruas e andar na calçada	Prender o animal na coleira e ficar longe do meio-fio.
109	É permitido transportar animais pelo lado de fora do veículo?	Sim	Sim, se bem amarrados	Não, somente em cima	Não
110	Furar uma fila é o mesmo que passar no semáforo com o sinal:	Amarelo	Verde	Azul	vermelho
111	Na fila do supermercado, a pessoa da sua frente está sendo atendida e você será o próximo, por isso você está atento. Isso significa, conforme o semáforo, que o sinal está:	Vermelho	Verde	Piscando	amarelo
112	As garagens de carros são bons lugares para brincar? Para que servem?	Sim, servem de esconderijo	Sim, caso estejam bem iluminadas	Não, servem para guardar coisas diversas	Não. Para guardar os veículos.
113	Porque não devemos brincar em estacionamentos?	Porque não tem parquinho	Podemos brincar de bicicleta sempre	Porque pode chover	Porque os veículos manobram em marcha ré, 
114	Qual é o sentido que o ciclista deve trafegar numa rodovia?	Qualquer sentido	Ciclistas não podem trafegar em rodovias	No sentido contrário dos carros 	No mesmo sentido dos carros.
115	É certo ou errado atravessar uma rua em que o semáforo para pedestres esteja na cor vermelha mas não esteja vindo nenhum veículo?	Certo	Certo se você for correndo	Errado apenas se algum veículo estiver passando	É errado
116	Posso atravessar a rua na faixa de pedestres quando o agente de trânsito está presente e me autoriza a fazê-lo?	Não   	Pode somente se ele apitar para você	Pode atravessar em qualquer lugar	Sim, pode
117	Uma criança recém nascida pode ser transportada no colo da mãe dentro do veículo?	Sim	Sim, se ela estiver amamentando	Não, deve ficar deitada no banco de trás	Não. Deve ficar na cadeirinha bebê conforto.
118	Qual o nome do veículo que faz o transporte de pessoas doentes ou acidentadas?	Carro de socorro	Carro de apoio	Transportador de doentes	Ambulância
119	Qual item sonoro é utilizado pelos bombeiros para abrir passagem no trânsito?	Buzina	Sino	Rádio	Sirene
120	Qual é a cor padrão dos caminhões e ambulâncias dos bombeiros?	Verde	Azul e branco	Amarelo	Vermelha