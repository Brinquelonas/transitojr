﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ARGameManager : MonoBehaviour {

    public Openable Fader;
    public Openable Frame;
    public ARQuiz ARQuiz;
    public QuizPlayer PlayerPrefab;
    public List<QuizPlayer> Players = new List<QuizPlayer>();
    public GameObject TargetsContainer;
    public GameObject WhiteTargetsContainer;
    public GameObject BlackTargetsContainer;
    public Button EndGameButton;
    public CurrentPlayerDisplay CurrentPlayerDisplay;

    public Scoreboard Scoreboard;
    public ReportScroll ReportScroll;
    public SendMailPopup SendMailPopup;
    public AlertPopup AlertPopup;

    public int _playerIndex;
    private string _log;

    private List<System.TimeSpan> _questionTimes = new List<System.TimeSpan>();
    private System.DateTime _questionStartTime;
    private System.DateTime _questionEndTime;

    private System.TimeSpan _gameTotalTime;
    private System.DateTime _gameStartTime;
    private System.DateTime _gameEndTime;

    public QuizPlayer CurrentPlayer
    {
        get
        {
            return Players[_playerIndex];
        }
    }

    private static ARGameManager _instance;
    public static ARGameManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ARGameManager>();

            return _instance;
        }
    }

    protected virtual void Awake()
    {
        _log += GameConfig.BookTrueName + "\n\n";

        if (GameConfig.Players == null || GameConfig.Players.Count <= 0)
        {
            GameConfig.Players = new List<PlayerConfig>() { new PlayerConfig("cho", new Sprite(), Color.red, Resources.Load<AudioClip>("Voice/geral/Jogar vermelho"), PlayerQRColor.Black), new PlayerConfig("chu", new Sprite(), Color.blue, Resources.Load<AudioClip>("Voice/geral/Jogar azul"), PlayerQRColor.White) };
        }

        for (int i = 0; i < GameConfig.Players.Count; i++)
        {
            QuizPlayer player = Instantiate(PlayerPrefab, transform);
            player.NameText.text = GameConfig.Players[i].Name;
            player.Image.sprite = GameConfig.Players[i].Sprite;
            player.Color = GameConfig.Players[i].Color;
            player.Audio = GameConfig.Players[i].Audio;
            player.QRColor = GameConfig.Players[i].QRColor;
            player.Log += GameConfig.Players[i].Name + "\n\n";

            player.gameObject.SetActive(false);

            Players.Add(player);
        }

        _playerIndex = -1;
        //NextPlayer();
        //Invoke("NextPlayer", 2f);
    }

    protected virtual void Start()
    {
        Fader.SetActive(false, () => 
        {
            Invoke("NextPlayer", 1f);
        });
    }

    public void QuestionFound()
    {
        SetTargetsActive(false);
        if (Frame != null)
            Frame.SetActive(true);
        ARQuiz.SetActive(true);

        ARQuiz.GetNewQuestion();

        _questionStartTime = System.DateTime.Now;
    }

    public void Answer(AnswerButton button)
    {
        ARQuiz.Answer(button);

        _questionEndTime = System.DateTime.Now;
        System.TimeSpan time = _questionEndTime.Subtract(_questionStartTime);
        string t = string.Format("{0:D2}m {1:D2}s",
                                 (int)time.TotalMinutes,
                                 time.Seconds);

        CurrentPlayer.TotalTime += (float)time.TotalSeconds;
        print(CurrentPlayer.TotalTime);
    }

    public virtual void NextPlayer()
    {
        _playerIndex++;
        _playerIndex = _playerIndex % Players.Count;

        if (Players[_playerIndex].RightAnswers >= 6)
        {
            for (int i = 0; i < Players.Count; i++)
            {
                if (Players[i].RightAnswers < 6)
                {
                    NextPlayer();

                    return;
                }
            }

            EndGame();
            return;
        }

        for (int i = 0; i < Players.Count; i++)
            Players[i].SetActive(i == _playerIndex);

        ARQuiz.SetActive(false);

        if (Frame != null)
            Frame.SetActive(false);

        if (Players.Count > 1)
            CurrentPlayerDisplay.SetActive(true);
        else
        {
            SetTargetsActive(true);
            EndGameButton.gameObject.SetActive(true);
        }
    }

    public void EndGame()
    {
        if (TargetsContainer != null)
            TargetsContainer.SetActive(false);
        ARQuiz.SetActive(false);
        ARQuiz.Feedback.SetActive(false);
        CurrentPlayerDisplay.SetActive(false);
        CurrentPlayer.SetActive(false);

        if (Frame != null)
            Frame.SetActive(true);

        List<QuizPlayer> players = new List<QuizPlayer>();
        players = Players.OrderByDescending((p) => p.RightAnswers).ThenBy((p) => p.WrongAnswers).ThenBy((p) => p.TotalTime).ToList();

        Scoreboard.Initialize(players);
        Scoreboard.SetActive(true);

        AudioPlayer.PlayVoiceGeneral("O jogador vencedor é", () => 
        {            
            AudioPlayer.Play(players[0].Audio);
        });

        EndGameButton.gameObject.SetActive(false);

        for (int i = 0; i < Players.Count; i++)
        {
            System.TimeSpan time = System.TimeSpan.FromSeconds(Players[i].TotalTime);
            string t = string.Format("{0:D2}m {1:D2}s",
                                     (int)time.TotalMinutes,
                                     time.Seconds);

            Players[i].Log += "\tRepostas corretas: " + Players[i].RightAnswers + "\n\tRespostas erradas: " + Players[i].WrongAnswers + "\n\tTempo total de resposta: " + t + "\n";

            _log += Players[i].Log + "\n";
        }
    }

    public void OpenReport()
    {
        Scoreboard.SetActive(false);
        ReportScroll.SetActive(true);
        ReportScroll.Text.text = _log.Trim();
    }

    public void OpenSendMailPopup()
    {
        ReportScroll.SetActive(false);
        SendMailPopup.SetActive(true);
    }

    public void SendMailButtonClicked()
    {
        SendMailPopup.SendMailButtonClicked(_log, () => 
        {
            SendMailPopup.SetActive(false);
            AlertPopup.SetActive(true, 0);
            AudioPlayer.PlayVoiceGeneral("Email enviado com sucesso");
            AlertPopup.Text.text = "E-mail enviado com sucesso!";
            AlertPopup.Buttons[0].onClick.AddListener(() =>
            {
                ReturnToMainMenu();
            });
        }, () => 
        {
            SendMailPopup.SetActive(false);
            AlertPopup.SetActive(true, 0);
            AudioPlayer.PlayVoiceGeneral("Opa verifique sua conexão");
            AlertPopup.Text.text = "Opa! Verifique sua conexão e tente novamente.";
            AlertPopup.Buttons[0].onClick.AddListener(() => 
            {
                SendMailPopup.SetActive(true);
                AlertPopup.SetActive(false);
            });
        });
    }

    public void ReturnToMainMenu()
    {
        if (Frame != null)
            Frame.SetActive(false);
        CurrentPlayerDisplay.SetActive(false);
        CurrentPlayer.SetActive(false);
        Scoreboard.SetActive(false);
        ReportScroll.SetActive(false);
        SendMailPopup.SetActive(false);
        AlertPopup.SetActive(false);
        EndGameButton.gameObject.SetActive(false);

        Fader.SetActive(true, () => 
        {
            SceneManager.LoadScene(1);
        });
    }

    public void LogCurrentPlayer(string log)
    {
        CurrentPlayer.Log += log;
    }

    public void SetTargetsActive(bool enabled)
    {
        if (TargetsContainer != null)
            TargetsContainer.SetActive(enabled);
    }
}
