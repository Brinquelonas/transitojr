﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARQuiz : Openable {

    public Text QuestionText;
    public QuestionsTSVReader TSVReader;
    public List<AnswerButton> AnswerButtons;
    public AnswerFeedback Feedback;

    private List<Question> _questions = new List<Question>();
    private Question _currentQuestion;

    public QuizPlayer CurrentPlayer
    {
        get
        {
            return ARGameManager.Instance.CurrentPlayer;
        }
    }

    private void Start()
    {
        /*TSVReader.ReadTSV(GameConfig.TSV);
        _questions = TSVReader.GetQuestions();*/
    }

    public void GetNewQuestion()
    {
        if (_questions.Count <= 0)
        {
            TSVReader.ReadTSV(GameConfig.TSV);
            _questions = TSVReader.GetQuestions();
        }
            //_questions = TSVReader.GetQuestions();

        _currentQuestion = _questions[Random.Range(0, _questions.Count)];
        _questions.Remove(_currentQuestion);

        QuestionText.text = _currentQuestion.Text;
        ARGameManager.Instance.LogCurrentPlayer("\t" + _currentQuestion.Text + "\n");

        List<string> answers = new List<string>(_currentQuestion.WrongAnswers);
        answers.RemoveAt(Random.Range(0, answers.Count));
        answers.Add(_currentQuestion.RightAnswer);

        List<int> usedIndexes = new List<int>();

        for (int i = 0; i < AnswerButtons.Count; i++)
        {
            int index;

            do
            {
                index = Random.Range(0, answers.Count);
            } while (usedIndexes.Contains(index));
            usedIndexes.Add(index);

            AnswerButtons[i].Text.text = answers[index];
            //answers.RemoveAt(index);
            AnswerButtons[i].SetActive(true);
        }

        AudioPlayer.Instance.PlayClip(_currentQuestion.Audio);
    }

    public void Answer(AnswerButton button)
    {
        bool correct = button.Text.text == _currentQuestion.RightAnswer;

        Feedback.SetActive(true, correct);

        ARGameManager.Instance.LogCurrentPlayer("\tResposta: " + _currentQuestion.RightAnswer + "\n");
        ARGameManager.Instance.LogCurrentPlayer("\tResposta dada: " + button.Text.text + "\n");


        for (int i = 0; i < AnswerButtons.Count; i++)
        {
            AnswerButtons[i].SetActive(false);
        }

        if (correct)
        {
            QuestionText.text = "Correto! Pode atravessar!";
            CurrentPlayer.RightAnswers++;
            AudioPlayer.Instance.PlayClip(Resources.Load<AudioClip>("Voice/geral/Resposta correta"));

            ARGameManager.Instance.LogCurrentPlayer("\tResposta correta!\n\n");
        }
        else
        {
            QuestionText.text = "Errado... Não atravesse";
            CurrentPlayer.WrongAnswers++;
            AudioPlayer.Instance.PlayClip(Resources.Load<AudioClip>("Voice/geral/Resposta errada"));

            ARGameManager.Instance.LogCurrentPlayer("\tResposta errada...\n\n");
        }
    }

}
