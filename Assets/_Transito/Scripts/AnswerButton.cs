﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : Openable {

    private Text _text;
    public Text Text
    {
        get
        {
            if (_text == null)
                _text = GetComponentInChildren<Text>();

            return _text;
        }
    }

    private Button _button;
    public Button Button
    {
        get
        {
            if (_button == null)
                _button = GetComponent<Button>();

            return _button;
        }
    }

    private void Awake()
    {
        Button.onClick.AddListener(() => 
        {
            ARGameManager.Instance.Answer(this);
        });
    }

}
