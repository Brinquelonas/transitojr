﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerFeedback : Openable {

    public GameObject PositiveFeedback;
    public GameObject NegativeFeedback;
    public Button OKButton;

    public AudioClip PositveSound, NegativeSound;

    private void Awake()
    {
        OKButton.onClick.AddListener(() => 
        {
            SetActive(false);
            ARGameManager.Instance.NextPlayer();
        });
    }

    public void SetActive(bool active, bool positive)
    {
        PositiveFeedback.SetActive(positive);
        NegativeFeedback.SetActive(!positive);

        SetActive(active);

        if (positive)
            AudioPlayer.Instance.PlayClip(PositveSound);
        else
            AudioPlayer.Instance.PlayClip(NegativeSound);
    }

}
