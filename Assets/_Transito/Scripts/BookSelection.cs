﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookSelection : Openable {

    public List<BookToggle> Toggles;

    private void OnEnable()
    {
        AudioPlayer.PlayVoiceGeneral("Selecione o livro");
    }

    public void OKButtonClicked()
    {
        for (int i = 0; i < Toggles.Count; i++)
        {
            if (Toggles[i].Toggle.isOn)
            {
                GameConfig.BookName = Toggles[i].Name;
                GameConfig.BookTrueName = Toggles[i].TrueName;
                GameConfig.TSV = Toggles[i].TSV;

                MainMenuManager.Instance.BookSelected();

                return;
            }
        }
    }

}
