﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarSizeController : MonoBehaviour {

    public Slider SizeSlider;
    public Transform Car;

    private void Awake()
    {
        SizeSlider.onValueChanged.AddListener((v) => 
        {
            Car.transform.localScale = Vector3.one * v;
        });
    }

}
