﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentPlayerDisplay : Openable {

    public Text NumberText;
    public Text NameText;
    public Image Frame;

    public Button OKButton;

    private void OnEnable()
    {
        QuizPlayer player = ARGameManager.Instance.CurrentPlayer;
        ARGameManager.Instance.CurrentPlayer.SetActive(false);
        ARGameManager.Instance.EndGameButton.gameObject.SetActive(false);

        NumberText.text = (player.RightAnswers + 1).ToString();
        NameText.text = player.NameText.text;
        Frame.color = player.Color;

        AudioPlayer.Instance.PlayClip(player.Audio);
    }

    private void Awake()
    {
        OKButton.onClick.AddListener(() => 
        {
            SetActive(false);
            ARGameManager.Instance.SetTargetsActive(true);
            ARGameManager.Instance.CurrentPlayer.SetActive(true);
            ARGameManager.Instance.EndGameButton.gameObject.SetActive(true);
        });
    }
}
