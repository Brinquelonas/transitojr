﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Openable : MonoBehaviour {

    public bool IsOpen
    {
        get
        {
            return gameObject.activeSelf;
        }
    }

    private PotaTween _tween;
    public PotaTween Tween
    {
        get
        {
            if (_tween == null)
                _tween = GetComponent<PotaTween>();
            if (_tween == null)
                _tween = PotaTween.Create(gameObject).
                    SetScale(Vector3.one * 0.75f, Vector3.one).
                    SetAlpha(0f, 1f).
                    SetDuration(0.2f).
                    SetEaseEquation(Ease.Equation.InSine);

            return _tween;
        }
    }

    public void SetActive(bool active)
    {
        SetActive(active, null);
    }

    public void SetActive(bool active, System.Action callback)
    {
        if (active)
        {
            gameObject.SetActive(true);
            Tween.Stop();
            Tween.Play(callback);
        }
        else
        {
            Tween.Stop();
            Tween.Reverse(() => 
            {
                if (callback != null)
                    callback();
                
                gameObject.SetActive(false);
            });
        }
    }
}
