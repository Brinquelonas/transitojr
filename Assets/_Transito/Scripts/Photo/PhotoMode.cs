﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhotoMode : MonoBehaviour {

    public Button Back;
    public Button Photo;
    public Toggle Freeze;
    public PotaTween PhotoFlash;
    public GameObject Car;
    public Transform CarParent;
    public Vector3 CarLocalPosition;
    public Vector3 CarLocalRotation;
    public Text FreezeButtonLabel, FreezeButtonLabelFrozen;

    public List<GameObject> ObjectsToHide;

    private EasyAR.ImageTrackerBaseBehaviour _tracker;

    void Start ()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
        Screen.autorotateToPortrait = true;
        Screen.autorotateToPortraitUpsideDown = true;
        //Fader.Instance.FadeOut();

        Back.onClick.AddListener(() =>
        {
            /*Back.GetComponent<PotaTween>().Reverse();
            Photo.GetComponent<PotaTween>().Reverse();

            Fader.Instance.FadeIn();

            Invoke("LoadMainMenu", 0.55f);*/

            //GetComponent<CameraController>().CameraTexture.Stop();
        });

        Photo.onClick.AddListener(() =>
        {
            foreach (GameObject obj in ObjectsToHide)
                obj.SetActive(false);

            CameraController.Instance.TakePhoto();
            //SoundManager.Instance.PlaySound("Photo");
            PhotoFlash.SetDelay(0.1f).Play(() =>
            {
                PhotoFlash.SetDelay(0f).Reverse();

                foreach (GameObject obj in ObjectsToHide)
                    obj.SetActive(true);
            });
        });

        Freeze.onValueChanged.AddListener(FreezeCar);
    }
	
	void Update ()
    {
		
	}

    void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    
    public void FreezeCar(bool isOn)
    {
        FreezeButtonLabel.gameObject.SetActive(!isOn);
        FreezeButtonLabelFrozen.gameObject.SetActive(isOn);

        if (isOn)
        {
            Car.transform.SetParent(null);
            //CarParent.gameObject.SetActive(false);

            _tracker = CarParent.gameObject.GetComponent<EasyAR.ImageTargetBehaviour>().Loaders[0];
            CarParent.gameObject.GetComponent<EasyAR.ImageTargetBehaviour>().Bind(null);
        }
        else
        {
            CarParent.gameObject.SetActive(true);
            Car.transform.SetParent(CarParent);
            Car.transform.localPosition = CarLocalPosition;
            Car.transform.localEulerAngles = CarLocalRotation;
            CarParent.gameObject.GetComponent<EasyAR.ImageTargetBehaviour>().Bind(_tracker);
        }
    }
}
