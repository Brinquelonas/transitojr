﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelection : Openable {

    public List<PlayerToggle> PlayerToggles;

    private void OnEnable()
    {
        AudioPlayer.PlayVoiceGeneral("Quem vai jogar");
    }

}
