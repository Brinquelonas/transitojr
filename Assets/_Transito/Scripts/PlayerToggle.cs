﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerToggle : MonoBehaviour {

    public Sprite Sprite;
    public Sprite SpriteOff;
    public Color Color;
    public AudioClip Audio;
    public PlayerQRColor QRColor;

    public string Name
    {
        get
        {
            return NameInputField.text;
        }
    }

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();

            return _toggle;
        }
    }

    private InputField _nameInputField;
    public InputField NameInputField
    {
        get
        {
            if (_nameInputField == null)
                _nameInputField = GetComponentInChildren<InputField>();
            return _nameInputField;
        }
    }

    private Image _image;
    public Image Image
    {
        get
        {
            if (_image == null)
                _image = GetComponent<Image>();

            return _image;
        }
    }

    private void Awake()
    {
        Toggle.onValueChanged.AddListener((v) => 
        {
            Image.sprite = (v) ? Sprite : SpriteOff;
            NameInputField.gameObject.SetActive(v);

            if (v)
            {
                AudioPlayer.Play(Audio);
            }
        });

        Image.sprite = SpriteOff;
        NameInputField.gameObject.SetActive(false);
    }

}
