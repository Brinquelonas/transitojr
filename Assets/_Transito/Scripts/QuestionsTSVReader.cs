﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Question
{
    public string Text;
    public List<string> WrongAnswers;
    public string RightAnswer;
    public string AudioTag;
    public AudioClip Audio;

    public Question(string text, List<string> wrongAnswers, string rightAnswer, string audioTag)
    {
        Text = text;
        WrongAnswers = wrongAnswers;
        RightAnswer = rightAnswer;
        AudioTag = audioTag;

        if (string.IsNullOrEmpty(GameConfig.BookName))
            GameConfig.BookName = "1o ano";

        Audio = Resources.Load<AudioClip>("Voice/" + GameConfig.BookName + "/" + AudioTag);
    }
}

public class QuestionsTSVReader : MonoBehaviour {

    public TextAsset TSV;
    public List<Question> Questions = new List<Question>();

    private static QuestionsTSVReader _instance;
    public static QuestionsTSVReader Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<QuestionsTSVReader>();

            return _instance;
        }
    }

    private void Awake()
    {
        if (GameConfig.TSV == null)
            GameConfig.TSV = TSV;
            //ReadTSV(TSV);
    }

    public void ReadTSV(TextAsset tsv)
    {
        TSV = tsv;

        string[] lines = TSV.text.Split(System.Environment.NewLine[0]);

        for (int i = 1; i < lines.Length; i++)
        {
            string[] contents = lines[i].Split("\t"[0]);

            string text = contents[1].Trim();

            List<string> wrongAnswers = new List<string>();
            for (int j = 0; j < 3; j++)
            {
                int index = 2 + j;
                if (!string.IsNullOrEmpty(contents[index].Trim()))
                    wrongAnswers.Add(contents[index].Trim());
            }

            string rightAnswer = contents[5].Trim();

            string audioTag = contents[0].Trim();

            Questions.Add(new Question(text, wrongAnswers, rightAnswer, audioTag));
        }
    }

    public List<Question> GetQuestions()
    {
        List<Question> questions = new List<Question>();

        for (int i = 0; i < Questions.Count; i++)
        {
            if (!string.IsNullOrEmpty(Questions[i].Text) && !string.IsNullOrEmpty(Questions[i].RightAnswer))
                questions.Add(Questions[i]);
        }

        return questions;
    }    
}
