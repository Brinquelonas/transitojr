﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuizGameManager : ARGameManager {

    protected override void Awake()
    {
        base.Awake();

        //ARQuiz.SetActive(false);

        CurrentPlayerDisplay.OKButton.onClick.AddListener(() => 
        {
            QuestionFound();
            //Invoke("QuestionFound", 0.5f);
        });
    }

    public override void NextPlayer()
    {
        base.NextPlayer();

        if (Players.Count <= 1 && CurrentPlayer.RightAnswers < 6)            
        {
            QuestionFound();
        }
    }
}
