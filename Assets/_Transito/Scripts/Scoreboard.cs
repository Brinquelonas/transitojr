﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoreboard : Openable {

    public Button OKButton;
    public AudioClip EndGameSound;
    public List<ScoreboardPlace> Places;

    private void Awake()
    {
        OKButton.onClick.AddListener(() => 
        {

        });
    }

    private void OnEnable()
    {
        if (EndGameSound != null)
            AudioPlayer.Play(EndGameSound, 0.5f);
    }

    public void Initialize(List<QuizPlayer> players)
    {
        for (int i = 0; i < Places.Count; i++)
        {
            if (i < players.Count)
            {
                Places[i].Image.sprite = players[i].Image.sprite;
                Places[i].ScoreText.text = players[i].RightAnswers.ToString() + " acertos";
                Places[i].SetActive(true);
            }
            else
                Places[i].gameObject.SetActive(false);
        }
    }

}
