﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardPlace : Openable {

    public Image Image;
    public Text ScoreText;

}
