﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TransitEventSelector : MonoBehaviour {

    [System.Serializable]
    public class SelectorEvent : UnityEvent { }

    public int Space;
    public GameObject AnsweredSpace;
    public GameObject InvalidSpace;
    public PlayerQRColor QRCOlor;
    public SelectorEvent OnClick = new SelectorEvent();

    private BoxCollider _collider;
    public BoxCollider Collider
    {
        get
        {
            if (_collider == null)
                _collider = GetComponent<BoxCollider>();
            return _collider;
        }
    }

    private void OnEnable()
    {
        if (ARGameManager.Instance.Players == null || ARGameManager.Instance.Players.Count == 0 || ARGameManager.Instance._playerIndex < 0)
            return;

        bool valid = ARGameManager.Instance.CurrentPlayer.QRColor == QRCOlor;

        AnsweredSpace.SetActive(Space < ARGameManager.Instance.CurrentPlayer.RightAnswers && valid);
        InvalidSpace.SetActive(Space > ARGameManager.Instance.CurrentPlayer.RightAnswers || !valid);

        //Collider.enabled = !AnsweredSpace.activeSelf && !InvalidSpace.activeSelf;
        Invoke("EnableCollider", 0.1f);
    }

    void EnableCollider()
    {
        Collider.enabled = !AnsweredSpace.activeSelf && !InvalidSpace.activeSelf;
    }

    public  void OnMouseUpAsButton()
    {
        if (InvalidSpace.activeSelf || AnsweredSpace.activeSelf)
            return;

        OnClick.Invoke();
    }

}
